<?php
namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;

abstract class Repository
{
    protected $self;

    /**
     * @return string
     */
    abstract protected function getModelClass();

    /**
     * @return Model
     */
    public function getModel()
    {
        if (empty($this->self)) {
            $className = $this->getModelClass();
            $this->self = new $className();
        }

        return $this->self;
    }
}
