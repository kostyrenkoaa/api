<?php

namespace App\Repositories;

use \App\Models\Priority;

/**
 * Class PriorityRepository
 * @package App\Repositories
 *
 * @method Priority getModel()
 */
class PriorityRepository extends Repository
{
    protected function getModelClass(): string
    {
        return Priority::class;
    }

    /**
     * @return string
     */
    public function getAllJson(): string
    {
        return $this->getModel()
            ->newQuery()
            ->get()
            ->keyBy('id')->toJson();
    }
}
