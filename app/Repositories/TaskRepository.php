<?php

namespace App\Repositories;

use App\Models\Task;

/**
 * Class TaskRepository
 * @package App\Repositories
 *
 * @method Task getModel()
 */
class TaskRepository extends Repository
{
    protected function getModelClass(): string
    {
        return Task::class;
    }

    /**
     * @param $id
     * @return array
     */
    public function getAllTaskForUser($id): array
    {
        $tasks = $this->getModel()
            ->newQuery()
            ->where('user_id', $id)
            ->get()
            ->jsonSerialize(JSON_UNESCAPED_UNICODE);
        return array_values($tasks);
    }
}
