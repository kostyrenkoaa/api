<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Priority
 * @package App\Models
 *
 * @property string title
 * @property integer id
 * @method static find(int $id)
 */
class Priority extends Model
{
    use HasFactory;
}
