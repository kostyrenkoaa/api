<?php

namespace App\Models;

use App\Models\Priority;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Task
 * @package App\Models
 *
 * @property string title
 * @property string priority_id
 * @property integer user_id
 * @property string status
 * @property array tags
 * @property Priority priority
 */
class Task extends Model
{
    use HasFactory;

    const STATUSES = ['inWork', 'closed'];

    protected $fillable = ['title', 'user_id'];

    public function priority()
    {
        return $this->belongsTo(Priority::class);
    }
}
