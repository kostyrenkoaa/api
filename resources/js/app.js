require('./bootstrap');

require('alpinejs');

import Vue from 'vue';
window.Vue = require('vue');

import 'bootstrap';
import 'bootstrap/dist/css/bootstrap-grid.css';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap-reboot.css';
import AppComponent from './components/App.vue';


const app = new Vue({
    el: '#app',
    components: {
        'app-component': AppComponent,

    }
});
